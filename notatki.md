
npx create-react-app . --template typescript

npm start

## Joel List
https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/


## Monorepo
https://en.wikipedia.org/wiki/Monorepo
https://github.com/lerna/lerna
https://nx.dev/


## UI Toolkits

https://js.devexpress.com/Overview/Widgets/

https://www.primefaces.org/primereact/
https://ant.design/components/button/
https://react-bootstrap.github.io/components/buttons/
https://material.angular.io/components/categories
https://blog.logrocket.com/top-11-react-ui-libraries-kits/
https://www.devexpress.com/
https://blueprintjs.com/


## Design Tokens
https://spectrum.adobe.com/page/design-tokens/#Introduction
https://material.io/design/color/the-color-system.html#color-usage-and-palettes
https://tailwindcss.com/
https://stitches.dev/

## Headless UI
https://headlessui.dev/react/listbox


## Storybook
https://www.componentdriven.org/
https://bradfrost.com/blog/post/atomic-web-design/
https://designsystemsrepo.com/design-systems/

https://storybook.js.org/docs/react/get-started/introduction

npx sb init
npm run storybook


## Playlist outline

mkdir -p src/playlists/pages/
mkdir -p src/playlists/components/

touch src/playlists/pages/PlaylistsPage.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx
touch src/playlists/components/PlaylistList.tsx


### Snippets
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

## Prototype / Mockup Desing
https://www.figma.com/
https://www.figma.com/design-systems/

https://www.adobe.com/pl/products/xd.html
https://www.sketch.com/

https://www.axure.com/

## Figma components + variants
https://www.figma.com/community/file/876022745968684318
https://help.figma.com/hc/en-us/articles/360038662654-Guide-to-Components-in-Figma


### UbiquitousLanguage

https://martinfowler.com/bliki/UbiquitousLanguage.html

- Components
- Data types
- Actions / Commands
- Tokens / Symbols
- Domain objects
- Diagram objects

## GIT Flow
https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow



## Search Screen outline

mkdir -p src/Search/pages/
mkdir -p src/Search/components/

touch src/Search/pages/SearchPage.tsx


## Swagger - Storybook for Backend

https://swagger.io/


## Agile

https://agilemanifesto.org/