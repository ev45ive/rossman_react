```tsx

// vdiv1 = React.createElement('div',{id:"tset_123"},
//      React.createElement('p',{style:{color:'red'}},'Ala ma kota')
// );
PersonComponent = function (props) {
  const [c, setC] = React.useState(0);

  return React.createElement(
    "div",
    { id: "tset_123" },
    React.createElement(
      "p",
      {
        style: { color: props.color },
        onClick: () => {
          setC(c + 1);
        },
      },
      props.user + " has " + props.pet + " " + c,
      React.createElement("input")
    )
  );
};

// app = ReactDOM.createRoot(root)
app.render(
  React.createElement(
    "div",
    {},
    React.createElement(PersonComponent, {
      user: "Alice",
      pet: "Cat",
      color: "green",
    }),
    React.createElement(PersonComponent, {
      user: "Bob",
      pet: "Horse",
      color: "red",
    }),
    2 > 2 && PersonComponent({ user: "Kate", pet: "Parrot", color: "blue" })
  )
);

```