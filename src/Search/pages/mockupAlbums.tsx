import { Album } from "../../model/Search";

export const mockupAlbums: Album[] = [
  {
    id: "123",
    name: "Test Album 123",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
  {
    id: "234",
    name: "Test Album 234",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/400/400" },
    ],
  },
  {
    id: "345",
    name: "Test Album 345",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/200/200" },
    ],
  },
  {
    id: "345",
    name: "Test Album 345",
    type: "album",
    images: [
      { height: 300, width: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
];
