import axios from "axios";
import React, { useState } from "react";
import { Button, Col, FormControl, InputGroup, Row } from "react-bootstrap";
import { Album, AlbumResponse } from "../../model/Search";
import { AlbumCard } from "../components/AlbumCard";
import { mockupAlbums } from "./mockupAlbums";

type Props = {};

const SearchPage = (props: Props) => {
  const [query, setQuery] = useState("batman");
  const [results, setResults] = useState(mockupAlbums);

  const search = () => {
    const res = axios.get<AlbumResponse[]>(
      "https://api.spotify.com/v1/search",
      {
        params: {
          type: "album",
          query,
        },
      }
    );
    res.then((res) => setResults(res.data));
  };

  return (
    <div>
      <h1 className="display-3">Search</h1>
      <Row>
        <InputGroup className="mb-3">
          <FormControl
            placeholder="Search"
            value={query}
            onChange={(e) => setQuery(e.target.value)}
            aria-label="Search"
            aria-describedby="basic-addon2"
          />
          <Button variant="outline-secondary" onClick={() => search()}>
            Search
          </Button>
        </InputGroup>
      </Row>

      <Row xs={1} md={4} className="g-0">
        {results.map((result, idx) => (
          <Col>
            <AlbumCard album={result} />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default SearchPage;
