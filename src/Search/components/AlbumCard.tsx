import React from "react";
import { Card } from "react-bootstrap";
import { Album } from "../../model/Search";

type Props = {
  album: Album;
};

export const AlbumCard = ({ album }: Props) => (
  <Card>
    <Card.Img variant="top" src={album.images[0].url} />

    <Card.Body>
      <Card.Title>{album.name}</Card.Title>
    
      {/* <Card.Text>
      This is a longer card with supporting text below as a natural
      lead-in to additional content. This content is a little bit
      longer.
    </Card.Text> */}

    </Card.Body>
  </Card>
);
