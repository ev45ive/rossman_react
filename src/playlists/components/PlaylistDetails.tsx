import React from "react";
import { Playlist } from "../../model/Playlist";

type Props = { playlist: Playlist };

const PlaylistDetails = ({ playlist }: Props) => {
  return (
    <div>
      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>
        <dt>Public:</dt>
        <dd
          style={{
            color: playlist.public == true ? "green" : "blue",
          }}
        >
          {playlist.public == true ? "Yes" : "No"}
        </dd>
        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>
    </div>
  );
};

export default PlaylistDetails;
