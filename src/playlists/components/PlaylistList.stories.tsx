import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import PlaylistList from "./PlaylistList";
import { playlistsData } from "../pages/playlistsData";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistList",
  component: PlaylistList,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistList>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistList> = (args) => (
  <PlaylistList {...args} />
);

export const EmptyList = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EmptyList.args = {
  playlists: [],
};

export const ExampleList = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ExampleList.args = {
  playlists: playlistsData,
};

export const SelectedItem = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SelectedItem.args = {
  playlists: playlistsData,
  selectedId: "123",
};
