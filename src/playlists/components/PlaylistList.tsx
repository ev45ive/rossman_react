import React from "react";
import ListGroup from "react-bootstrap/esm/ListGroup";
import { Playlist } from "../../model/Playlist";

type PlaylistListProps = {
  /**
   * List of playlists
   */
  playlists: Playlist[];
  /**
   * Currently selected playlist id
   */
  selectedId?: Playlist["id"];
  /**
   * Action on select
   * @param id Playlist we want to select id 
   */
  onSelected(id: string): void;
  onRemove(id: string): void;
};

const PlaylistList = ({
  playlists,
  onSelected,
  selectedId,
  onRemove,
}: PlaylistListProps) => {
  if (playlists.length === 0)
    return (
      <ListGroup>
        <ListGroup.Item>No playlists</ListGroup.Item>
      </ListGroup>
    );

  return (
    <div>
      <ListGroup>
        {playlists.map((playlist) => (
          <ListGroup.Item as="button"
            active={playlist.id === selectedId}
            onClick={() => {
              onSelected(playlist.id);
            }}
          >
            {playlist.name}
            <span
              className="close float-end"
              onClick={(event) => {
                event.stopPropagation()
                onRemove(playlist.id)}}
            >
              &times;
            </span>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  );
};

export default PlaylistList;
