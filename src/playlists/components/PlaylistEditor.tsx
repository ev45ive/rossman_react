import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Playlist } from "../../model/Playlist";

type Props = {
  playlist: Playlist;
  onSave: (draft: Playlist) => void;
};

const PlaylistEditor = ({ onSave, playlist }: Props) => {
  const [playlistName, setPlaylistName] = useState(playlist.name);

  return (
    <div>
      PlaylistEditor
      <div className="form-group">
        <label>Name</label>
        <input
          type="text"
          name="playlist_name"
          id="playlist_name"
          className="form-control"
          placeholder=""
          value={playlistName}
          onChange={(e) => setPlaylistName(e.target.value)}
        />
        <small id="helpId" className="text-muted">
           
        </small>
      </div>

      <Button onClick={() => onSave({
        ...playlist,
        name: playlistName
      })}>Save</Button>
      
    </div>
  );
};

export default PlaylistEditor;
