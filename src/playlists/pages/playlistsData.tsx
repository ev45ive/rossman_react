import { Playlist } from "../../model/Playlist";

export const playlistsData: Playlist[] = [
  {
    id: "123",
    name: "Playlista 123",
    public: false,
    description: "Best playlist",
  },
  {
    id: "234",
    name: "Playlista 234",
    public: false,
    description: "Best playlist",
  },
  {
    id: "345",
    name: "Playlista 345",
    public: false,
    description: "Best playlist",
  },
];
