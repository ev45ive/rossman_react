import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import PlaylistsPage from "./PlaylistsPage";
import { within } from "@testing-library/react";
import { userEvent } from "@storybook/testing-library";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistsPage",
  component: PlaylistsPage,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistsPage>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistsPage> = (args) => (
  <PlaylistsPage {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {};

Primary.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const loginButton = await canvas.getByRole('button', { name: /Playlista 234/i });
  await userEvent.click(loginButton);


};
