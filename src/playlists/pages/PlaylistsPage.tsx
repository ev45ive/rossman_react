import axios from "axios";
import React, { useEffect, useReducer, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Playlist } from "../../model/Playlist";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistEditor from "../components/PlaylistEditor";
import PlaylistList from "../components/PlaylistList";
import {
  reducer,
  initialState,
  SelectAction,
  RemoveAction,
  UpdateAction,
  LoadedAction,
} from "./playlistsReducer";

type Props = {};

/// =======================

const PlaylistsPage = (props: Props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const selectPlaylist = (id: string) => dispatch(SelectAction(id));
  const removePlaylist = (id: string) => dispatch(RemoveAction(id));
  const savePlaylist = (draft: Playlist) => dispatch(UpdateAction(draft));

  useEffect(() => {
    const res = axios.get("playlists.json");
    res.then((res) => dispatch(LoadedAction(res.data)));
  }, []);

  return (
    <div>
      <h3 className="display-3">Playlists 2</h3>
      <Row>
        <Col>
          <PlaylistList
            playlists={state.data}
            onRemove={removePlaylist}
            onSelected={selectPlaylist}
            selectedId={state.selected?.id}
          />
        </Col>
        <Col>
          {state.selected && (
            <>
              <PlaylistDetails playlist={state.selected} />
              <PlaylistEditor
                playlist={state.selected}
                onSave={savePlaylist}
              ></PlaylistEditor>
            </>
          )}
        </Col>
      </Row>

      {/* <pre>{JSON.stringify(state, null, 2)}</pre> */}
    </div>
  );
};

export default PlaylistsPage;
