import { Playlist } from "../../model/Playlist";
import { playlistsData } from "./playlistsData";

interface State {
  data: Playlist[];
  selected?: Playlist;
}
export const initialState: State = {
  data: playlistsData,
  selected: playlistsData[0],
};

type Actions =
  | ReturnType<typeof SelectAction>
  | ReturnType<typeof RemoveAction>
  | ReturnType<typeof UpdateAction>
  | ReturnType<typeof LoadedAction>;

export const reducer = (
  state: State = initialState,
  action: Actions
): State => {
  console.log(action);

  switch (action.type) {
    case "PLAYLISTS_LOADED":
      return {
        ...state,
        data: action.payload.data,
      };
    case "PLAYLISTS_REMOVE":
      return {
        ...state,
        data: state.data.filter((p) => p.id !== action.payload.id),
        selected:
          state.selected?.id === action.payload.id ? undefined : state.selected,
      };
    case "PLAYLISTS_SELECT":
      return {
        ...state,
        selected: state.data.find((p) => p.id === action.payload.id)!,
      };
  }

  return state;
};

export const LoadedAction = (data: Playlist[]) =>
  ({ type: "PLAYLISTS_LOADED", payload: { data } } as const);

export const SelectAction = (id: string) =>
  ({ type: "PLAYLISTS_SELECT", payload: { id } } as const);

export const RemoveAction = (id: string) =>
  ({ type: "PLAYLISTS_REMOVE", payload: { id } } as const);

export const UpdateAction = (draft: Playlist) =>
  ({ type: "PLAYLISTS_UPDATE", payload: { draft } } as const);
