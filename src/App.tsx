import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Container, Row, Col } from "react-bootstrap";
import PlaylistsPage from "./playlists/pages/PlaylistsPage";

function App() {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <PlaylistsPage />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
